# RM

- goftim k Relational Mapper ha baraye sade kardan bargharary relation beyn relational DB ha bekar miran .

- it's alot of work storing data into the relational database .

- there is usually a number of **third-party library** that will come between the **actual SQL** and **calling application** .

- in the **OOP language** these are often called **Object-Relational mappers(ORMs)** .

- ORM ha datvahe mian map mikonan (negasht mikonn) **relational database tables** and the **objects in the programming language** .

- ORM ha faghat makhsos OOP language ha nistan .

- toye zaban golang hham ma RM darim **Relational mappers** ; ok nist k beheshon begim ORM , **RM** ok tare toye golang .

- _Sqlx_ , _Gorm_

- chizi k fahmidam ine k aksar developer ha tarjih midan az sqlx estefade konan ta Gorm chon doc on kheily clear nist ...

- [redditPost](https://www.reddit.com/r/golang/comments/14zavlj/what_are_you_using_for_database_when_building_api/?utm_source=share&utm_medium=android_app&utm_name=androidcss&utm_term=14&utm_content=1)

- [someNotes](sarnik80/realationalmappers#1)
