package main

import (
	"fmt"

	"github.com/jmoiron/sqlx"

	_ "github.com/lib/pq"
)

type Post struct {
	Id         int
	Content    string
	AuthorName string `db:"author"`
}

var (
	Db *sqlx.DB
)

func init() {

	var err error

	connectionSTR := "postgres://root:j06ar8KkODkxhOon0y2LEe2y@arthur.iran.liara.ir:30135/s?sslmode=disable"
	Db, err = sqlx.Open("postgres", connectionSTR)

	if err != nil {
		fmt.Println(err)
	}

}

func main() {

	post := Post{Content: "Hello!", AuthorName: "Sau Sheong"}
	post.Create()

	pst, err := GetPost(4)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(pst)

}

func GetPost(id int) (pst Post, err error) {

	pst = Post{}

	err = Db.QueryRowx("select id , content , author from posts  where  id = $1", id).StructScan(&pst)
	if err != nil {

		return
	}

	return

}

func (pst *Post) Create() (err error) {
	err = Db.QueryRow("insert into posts (content , author ) values ($1, $2) returning id", pst.Content, pst.AuthorName).Scan(&pst.Id)

	return

}
