# Sqlx

- sqlx is a **third party library**

- provides a set of useful extentions to the database/sql package

- it uses the same interfce

- mitonim az struct tag ha estefade konim

- sqlx makes life easier with StructScan method

how to get library :

```bash
go get "github.com/jmoiron/sqlx"
```

## Defference between Sqlx & database/sql

- ```go
   import "github.com/jmoiron/sqlx"   // instead of importing  database/sql
  ```
- **StructScan Method** maps struct field name ro be table e k lowercase bashe field hash ... look at the code below :

- ```go
    func GetPost(id int) (pst Post, err error) {

        pst = Post{}

        err = Db.QueryRowx("select id , content , author from posts  where  id = $1", id).StructScan(&pst)
        if err != nil {

            return
        }

    return

    }
  ```

- ma toye type e k tarif kardim omadim va field **AuthorName** estefade kardim bejaye **Author** k toye db hast bas in irad migire az ma chon nemitone in ro map kone be on
  vali agar az **struct tag** ha estefade konim ok mishe

  ```go
      type Post struct {
          Id         int
          Content    string
          AuthorName string `db:"author"`

      }
  ```

- instead of using sql.DB, now we use sqlx.DB

- there are a few features in **sqlx**

- the next library **(GORM)** hides database/sql package and uses an ORM mechanism instead .
